package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nombres", nullable = false, length = 100)
	private String nombres;

	@Column(name = "primer_apellido", nullable = false, length = 100)
	private String primerApellido;

	@Column(name = "segundo_apellido", length = 100)
	private String segundoApellido;

	@Column(name = "documento_identidad", length = 20)
	private String documentoIdentidad;

	@Column(name = "lugar_emision", length = 5)
	private String lugarEmision;

	@Column(name = "nacionalidad", length = 30)
	private String nacionalidad;

	@Column(name = "sexo", length = 10)
	private String sexo;

	@Column(name = "fecha_nacimiento", length = 10)
	private String fechaNacimiento;

	@Column(name = "calle", length = 100)
	private String calle;

	@Column(name = "numero", length = 10)
	private String numero;

	@Column(name = "departamento", length = 50)
	private String departamento;

	@Column(name = "codigo_cliente", nullable = false, length = 20)
	private String codigoCliente;

	

}
