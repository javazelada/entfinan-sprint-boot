package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.example.demo.model.Cliente;
import com.example.demo.service.ClienteService;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
@RequestMapping("/api/cliente")
public class ClienteController {

	private ClienteService clienteService;

	public ClienteController(ClienteService clienteService) {
		super();
		this.clienteService = clienteService;
	}
	
	
	@PostMapping()
	public ResponseEntity<Cliente> guardarCliente(@RequestBody Cliente cliente) {
		return new ResponseEntity<Cliente>(clienteService.guardarCliente(cliente), HttpStatus.CREATED);
	}

	@GetMapping
	public List<Cliente> obtenerClientes() {
		return clienteService.obtenerClientes();
	}

	@GetMapping("{id}")
	public ResponseEntity<Cliente> obtenerClienteId(@PathVariable("id") long clienteId) {
		return new ResponseEntity<Cliente>(clienteService.obtenerClienteId(clienteId), HttpStatus.OK);
	}

	@PutMapping("{id}")
	public ResponseEntity<Cliente> actualizarCliente(@PathVariable("id") long id, @RequestBody Cliente cliente) {		
		return new ResponseEntity<Cliente>(clienteService.actualizarCliente(cliente, id), HttpStatus.OK);
	}

	@DeleteMapping("{id}")
	public ResponseEntity<String> borrarCliente(@PathVariable("id") long id ) {
		clienteService.borrarCliente(id);
		return new ResponseEntity<String>("Cliente eliminado", HttpStatus.OK);
	}
	
}
