package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Cliente;

public interface ClienteService {
	Cliente guardarCliente(Cliente cliente);

	List<Cliente> obtenerClientes();

	Cliente obtenerClienteId(long id);

	Cliente actualizarCliente(Cliente cliente, long id);

	void borrarCliente(long id);
}
