package com.example.demo.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;

import javax.websocket.ClientEndpoint;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Cliente;
import com.example.demo.repository.ClienteRepository;
import com.example.demo.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {
	private ClienteRepository clienteRepository;

	public ClienteServiceImpl(ClienteRepository clienteRepository) {
		super();
		this.clienteRepository = clienteRepository;
	}

	@Override
	public Cliente guardarCliente(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	@Override
	public List<Cliente> obtenerClientes() {
		return clienteRepository.findAll();
	}

	@Override
	public Cliente obtenerClienteId(long id) {

		return clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cliente", "Id", id));
	}

	@Override
	public Cliente actualizarCliente(Cliente cliente, long id) {
		Cliente clienteExistente = clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("cliente", "id", id));
		clienteExistente.setNombres(cliente.getNombres());
		clienteExistente.setPrimerApellido(cliente.getPrimerApellido());
		clienteExistente.setSegundoApellido(cliente.getSegundoApellido());
		clienteExistente.setCodigoCliente(cliente.getCodigoCliente());
		clienteExistente.setCalle(cliente.getCalle());
		clienteExistente.setDepartamento(cliente.getDepartamento());
		clienteExistente.setDocumentoIdentidad(cliente.getDocumentoIdentidad());
		clienteExistente.setFechaNacimiento(cliente.getFechaNacimiento());
		clienteExistente.setLugarEmision(cliente.getLugarEmision());
		clienteExistente.setNacionalidad(cliente.getNacionalidad());
		clienteExistente.setNumero(cliente.getNumero());
		clienteExistente.setSexo(cliente.getSexo());
		clienteRepository.save(clienteExistente);
		return clienteExistente;
	}

	@Override
	public void borrarCliente(long id) {
		clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("cliente", "id", id));
		clienteRepository.deleteById(id);;	
	}

}
